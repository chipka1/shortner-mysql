<?php

namespace App\Helpers;

use App\Link;

class LinkHelper
{
    const URL_LENGTH = 6;

    public static function getUniqueRouteName(): string
    {
        $isUsed = true;

        while($isUsed) {
            $str = str_random(self::URL_LENGTH);
            $isUsed = Link::whereRouteName($str)->exists();
        }

        return $str;
    }
}
