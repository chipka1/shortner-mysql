<?php

namespace App\Http\Controllers;

use App\Click;
use App\Link;
use App\Helpers\LinkHelper;
use App\Http\Requests\StoreLink;
use Illuminate\Http\Request;

class LinkController extends Controller
{
    public function createLink(StoreLink $request)
    {
        $extarnalUrl = $request->external_url;
        $link = Link::whereExternalUrl($extarnalUrl)->first();
        
        if ($link === null) {
            $link = Link::create([
                'route_name' => LinkHelper::getUniqueRouteName(),
                'external_url' => $extarnalUrl
            ]);
        }

        if ($link) {
            return response()->json([
                'status' => true,
                'message' => 'Link was successfully created!',
                'link' => env('APP_URL') . $link->route_name
            ]);
        }
            return response()->json([
                'status' => false,
                'message' => 'Something was wrong!'
            ]);
    }

    public function getData()
    {
        $links = Link::withCount(['clicks'])->get()->map(function ($link) {
            $link->clicks = $link->getClickIps();
            $link->url = env('APP_URL') . $link->route_name;
            
            return $link;
        });

        return response()->json([
            'links' => $links
        ]);
    }

    public function redirect(Request $request, string $routeName)
    {
        $link = Link::whereRouteName($routeName)->first();

        if ($link !== null) {
            Click::create([
                'link_id' => $link->id,
                'ip' => $request->getClientIp()
            ]);

            return redirect()->away($link->external_url);
        }

        abort(404);
    }
}
