<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Link extends Model
{
    protected $fillable = [
        'route_name', 'external_url'
    ];

    public function clicks() 
    {
        return $this->hasMany(Click::class);
    }

    public function getClickIps()
    {
        return $this->clicks()->select('ip')->groupBy('ip')->get();
    }
}
