import Vue from 'vue';
import ShortnerForm from './components/ShortnerForm.vue';
import ShortnerStatisticsBlock from './components/ShortnerStatisticsBlock.vue';

Vue.component('shortner-form', ShortnerForm);
Vue.component('shortner-statistics', ShortnerStatisticsBlock);

// eslint-disable-next-line
const app = new Vue({
    el: '#app'
});
