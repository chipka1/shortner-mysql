<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="_token" content="{{ csrf_token() }}">
        <link href="{{ mix('/assets/css/app.css')}}" rel="stylesheet">
        <script src="{{ mix('/assets/js/vendor.js') }}"></script>
        <title>Shortner</title>
    </head>
    <body>
        <div id="app">
            <div class="container d-flex justify-content-center flex-column align-items-center pt-4">
                <shortner-form action="{{ route('link.create') }}"></shortner-form>
                <shortner-statistics action="{{ route('link.data') }}"></shortner-statistics>
            </div>
        </div>
    </body>
    <script src="{{ mix('/assets/js/app.js') }}"></script>
</html>
